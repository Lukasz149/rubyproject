class Event < ApplicationRecord
    validates :artist, :presence => true
    validates :number_of_seats, :presence => true
    validate :event_date_not_from_past
    has_many :tickets

    def event_date_not_from_past
      if event_date < Date.today
        errors.add('The date of the event can not be from the past', '')
      end
    end
end
