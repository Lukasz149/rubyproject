class Ticket < ApplicationRecord
validates :name, :presence => true, :length => { :minimum => 6 }
validates :email_address, :presence => true
validates :price, :presence => true
validates :event_id, :presence => true
validates :address, :presence => true
validates :phone, :presence => true
validates :user_id, :presence => true
validates :seat_id_seq, :presence => true
validate :price_range
validate :user_balance_to_low
validate :number_of_seats
validate :max_ticket_for_user
belongs_to :event
belongs_to :user

  def user_balance_to_low
    if price.present? && user.present? && user.balance < price
      errors.add('The price is greater than your account balance', '')
    end
  end

  def price_range
    if price.present? && (price < event.price_low || price > event.price_high)
      errors.add('The price is uncorrect', '')
    end
  end

  def number_of_seats
    if event.number_of_seats == 0
      errors.add('No tickets available for the event', '')
    end
  end

  def max_ticket_for_user
    @number_of_tickets = user.tickets.where(event_id: event_id).count
    # @number_of_tickets = User.count(:conditions => "user_id = user_id AND event_id = event_id")
    if @number_of_tickets > 4
      errors.add('You can not buy more tickets for this event (5 max)', '')
    end
  end
end
