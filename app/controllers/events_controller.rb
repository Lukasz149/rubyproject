class EventsController < ApplicationController
  # before_action :check_logged_in, :only => [:new, :create]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def index
    @events = Event.all
  end

  def show
  end

  def new
    @event = Event.new
  end

  def edit
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      flash[:komunikat] = 'Event successfully created.'
      redirect_to "/events/#{@event.id}"
    else
      render 'new'
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    # respond_to do |format|
      if @event.update(event_params)

        flash[:komunikat] = 'Event saccessfully updates.'
        redirect_to "/events/#{@event.id}"

      else
        redirect_to "/events/#{@event.id}/edit"
      end
  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def check_logged_in
    authenticate_or_request_with_http_basic("Ads") do |username, password|
      username == "admin" && password == "admin"
    end
  end
  def event_params
    params.require(:event).permit(:artist, :description, :price_low, :price_high, :event_date, :number_of_seats)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end


end
