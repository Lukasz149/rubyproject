Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'registrations' }
  #get 'events/index'
  resources :events

  #get 'events/new'

  # get 'events/create'
  post '/events' => 'events#create'
  patch '/events/:id' => 'events#update'

  # get 'events/show'
  #get 'events/:id' => 'events#show'

  resources :tickets
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "events#index"
end
