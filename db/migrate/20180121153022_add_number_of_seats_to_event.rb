class AddNumberOfSeatsToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :number_of_seats, :integer, :default => 0
  end
end
