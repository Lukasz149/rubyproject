class Empty3 < ActiveRecord::Migration[5.1]
  def change
    remove_reference :tickets, :user, foreign_key: true
  end
end
